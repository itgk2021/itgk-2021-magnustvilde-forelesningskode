'''VARIABLER'''

# opprette variabler, lag en variabel med et tall
tall = 4

# lag en variabel med tekst
tekst = 'Dette er en tekst'

# lag en variabel med et kommatall
desimalTall = 6.21

# legge sammen variabler
total = tall+desimalTall
print(total)

storTekst = tekst*3
print(storTekst)