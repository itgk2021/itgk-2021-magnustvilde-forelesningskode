'''
UKE 36
Oppsummering
'''

#standardfunksjoner
a, b, c, d, e, f = -2, 4, 5, -1.5, 0, 6
tekst = 'Hva er lengden av denne strengen?'
maks = max(a,b,c,d,e,f)
mini = min(a,b,c,d,e,f)
absolutt = abs(d)
avrundet = round(d)
lengde = len(tekst)

#if-setning
if (4*5 > 15):
    print('4*5 er større')

#if-else
trafikaltGrunnkurs = True
if (trafikaltGrunnkurs):
    print('Du kan ta lappen!')
else:
    print('Du må først ta trafikalt grunnkurs')

#if-elif-else
penger, godteri, brus = 30, 30, 20
if (penger >= godteri+brus):
    print('Kjøp begge')
elif (penger >= godteri):
    print('Kjøp godteri')
elif (penger >= brus):
    print('Kjøp brus')
else:
    print('Ikke kjøp')

# betingelser
a,b,c = 5,3,5

#verdien til alle disse variablene blir Boolean-verdier
mindre = (a<b)
storre = (a>b)
mindreLik = (a <= b)
storreLik = a>=c
lik = a == c
ulik = a != c

#logiske uttrykk/logiske operatorer
sant = True
usant = False
sjekk1 = sant and usant
sjekk2 = sant or usant
sjekk3 = not usant
sjekk4 = not sant
sjekk5 = (sant and usant) or (not usant)
