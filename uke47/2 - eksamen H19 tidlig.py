'''4 - eksamen H19 0900-1300'''

register = {'N64O':[['laks', 511], ['hyse', 342]], 'Z4F':[['torsk', 233], ['hyse', 122]]}

'''
Oppgave 3a
Få inn ei streng med tall og bokstaver, skal sjekke om strengen er på følgende format:
<1 bokstav><1-2 siffer><1-2 bokstavar>

Returner True dersom strengen følger formatet, og False dersom den ikke følger formatet.
Skriv funksjonen check_registration som skal ta imot eit registreringsnummer og returnere
om det er skrive i samsvar med retningslinjene i fiskeriregisteret.

Følgjande registreringsnummer skal til dømes òg returnere True: N4A, B46MC, P53V, Z4HB.
Følgjande registreringsnummer skal returnere False: NR5H, P557J, 672NH.
'''
def check_registration(regnummer):
    alfa = 'ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ'
    tall = '1234567890'
    if (regnummer[0] in alfa) and (regnummer[1] in tall) and (regnummer[-1] in alfa):
        if len(regnummer) == 3:
            return True
        elif len(regnummer) == 4:
            if (regnummer[2] in tall) or (regnummer[2] in alfa):
                return True
        elif len(regnummer) == 5:
            if (regnummer[2] in tall) and (regnummer[3] in alfa):
                return True
    return False


print('Skal bli True:', check_registration('N64Ø'))
print('Skal bli False:', check_registration('NN4Ø'))
print('Skal bli True:', check_registration('N4A'))
print('Skal bli True:', check_registration('B46MC'))
print('Skal bli True:', check_registration('Z4HB'))
print('Skal bli False:', check_registration('NR5H'))
print('Skal bli False:', check_registration('P5557'))

'''
Oppgave 3b
Hent ut hvor mye av en viss fisk.
Skriv funksjonen fish_amount som tek parameterane store og kind.
Dersom fisken ikke er lagt inn skal 0 returneres.
'''
store = [['torsk', 200], ['sei', 100]]

def fish_amount(store, kind):
    for fish in store:
        if fish[0] == kind:
            return fish[1]
    return 0

print(fish_amount(store, 'sei'))
print(fish_amount(store, 'torsk'))
print(fish_amount(store, 'månefisk'))

'''
Oppgave 3c
Lageret skal oppdaterast når det kjem inn meir fisk. Dette vert oppgitt som ei liste med
fiskeslag og talet på kilo: ['torsk', 200]. Lag eit nytt element viss fisketypen ikkje allereie
finst i lageret.
Skriv funksjonen add_fish, som tar store og ei liste som inputparameterar og returnerer det
oppdaterte lageret.
'''

store = [['torsk', 200], ['sei', 100]]

def add_fish(store, fish):
    for entry in store:
        if fish[0] == entry[0]:
            entry[1] += fish[1]
            return store
    store.append(fish)
    return store

print(store)
add_fish(store, ['torsk', 100])
add_fish(store, ['hyse', 70])
print(store)
'''Oppgave 3d
Ein fiskar kan fange mange typar fisk på ein gong - og ynskjer dermed å rapportere inn dette. Fleire typer fisk
vert rapportert inn som ei todimensjonal liste, på same format som store:
[['type', mengde], ['type',mengde], ['type', mengde]]
Skriv funksjonen add_much_fish som tek store og ei 2D-liste som inputparameterar.'''
def add_much_fish(store, fishlist):
    for fish in fishlist:
        add_fish(store, fish)
    return store

print(store)
add_much_fish(store, [['kveite', 120], ['torsk', 200]])
print(store)

'''
Oppgave 3e

Kva og kor mykje som skal fjernast vert sende med som parameter, på formatet ['type', mengde].
Lag funksjonen remove_fish som har parameterane store og remove.
Funksjonen skal fjerne den gitte mengda fisk frå lageret (store). Dersom det skulle gjerast eit forsøk på å fjerne
meir frå lageret enn det faktisk finst, så skal det givast beskjed om dette. I så fall skal ingen fisk fjernast når
funksjonen vert kalla. Du finn eit eksempel på dette ved spørjing etter kveite under.
'''
def remove_fish(store, remove):
    if fish_amount(store, remove[0]) >= remove[1]:
        add_fish(store, [remove[0], -remove[1]])
    else:
        print('Det finnes ikke nok', remove[0], 'igjen.')
    return store

print(store)
remove_fish(store, ['torsk', 300])
remove_fish(store, ['kveite', 200])
print(store)
'''
Oppgave 3f

Skriv funksjonen read_fish_from_file. Funksjonen tek inn ein dictionary register - denne er beskriven i første
del av oppgåveteksta. Dersom det ikkje eksisterer ein båt med båtnamnet i registeret frå før, og båtnamnet
stemmer med namnesjekken i oppgåve a skal det opprettast ein ny nøkkel på denne og fangsten som er
rapportert skal leggjast inn. Dersom båtnavnet ikkje er korrekt bygd opp skal det ikkje leggjast til i registeret,
men funksjonen skal i staden skrive ut:
'Båtkode' var ugyldig - hopper over fangstrapportering.
N64Ø:torsk:343
N434:torsk:200
Z4F:torsk:120
N64Ø:torsk:200
N64Ø:hyse:110
NB4A:kveite:100
Z4F:hyse:120
N64Ø:kveite:300
'''


register = {'N64O':[['laks', 511], ['hyse', 342]], 'Z4F':[['torsk', 233], ['hyse', 122]]}

def read_fish_from_file(register):
    with open('fishy.txt', 'r') as fil:
        linjer = fil.readlines()
    for linje in linjer:
        innhold = linje.strip('\n').split(':')
        boat, fish, amount = innhold[0], innhold[1], int(innhold[2])
        if check_registration(boat):
            if boat in register.keys():
                add_fish(register[boat], [fish, amount])
            else:
                register[boat] = [[fish, amount]]
        else:
            print(boat + ' var ugyldig - hopper over fangstrapportering.')
    return register

read_fish_from_file(register)
print(register)


'''
Oppgave 3g

'''