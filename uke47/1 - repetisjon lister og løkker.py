'''1 - repetisjon lister og løkker'''

personer = []
# lag en tom liste 'personer'. Lag en funksjon legg_til_person som tar inn tre parametre,
# personer, navn og alder, som legger navn og alder i en egen liste inni personer. Returner personer

def legg_til_person(personer, navn, alder):
    personer.append([navn, alder])
    return personer

# gjør noen funksjonskall slik at vi har noen personer.
legg_til_person(personer, 'Ola', 23)
legg_til_person(personer, 'Kari', 43)
legg_til_person(personer, 'Knut', 56)
legg_til_person(personer, 'Mari', 10)
print(personer)

# Lag en funksjon legg_til_bosted som tar inn den to-dimensjonale listen personer, navn på en person og bosted.
# Legg til bosted på slutten av den tilhørende personen sin liste. Returner personer
def legg_til_bosted(personer, navn, bosted):
    for person in personer:
        if person[0] == navn:
            person.append(bosted)
    return personer

legg_til_bosted(personer, 'Knut', 'Trondheim')
legg_til_bosted(personer, 'Mari', 'Oslo')
print(personer)

# Lag en funksjon som skriver ut verdiene til en person på en fornuftig måte vha. en løkke.
# funksjonen skal fungere både på personer som har registrert bosted og på personer som ikke har det.
def skriv_personer(personer):
    for person in personer:
        try:
            print('Navn:', person[0])
            print('Alder:', person[1])
            print('Bosted:', person[2])
        except:
            print('Ikke noe registrert bosted.')
#         for element in person:
#             print(element)

skriv_personer(personer)

# Lag en funksjon tar inn personer og som skriver ut alle registrerte bosteder
def skriv_bosteder(personer):
    bosteder = []
    for person in personer:
        if len(person) == 3:
            bosteder.append(person[2])
#         try:
#             bosteder.append(person[2])
#         except:
#             pass
    return bosteder

print(skriv_bosteder(personer))

# Lag en funksjon som tar inn personer og regner ut gjennomsnittsalderen på personene.
def gj_alder(personer):
    total = 0
    for person in personer:
        total += person[1]
    return total/len(personer)

print(gj_alder(personer))
