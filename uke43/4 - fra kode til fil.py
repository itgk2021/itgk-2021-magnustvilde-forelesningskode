'''fra struktur i kode til fil - to-dimensjonal liste'''

personer = [['Ola', 45], ['Kari', 23], ['Hermann', 87],
            ['Mikkel', 34], ['Nora', 16], ['Harald', 37],
            ['Anna', 65], ['Thea', 39]]

# Skriv listen til en tekstfil, på følgende format:
'''
navn, alder
navn, alder
...
'''

# Åpne fil med mulighet for å skrive
with open('4_personer.txt', 'a') as fil:
    # – Iterere over listene
    for person in personer:
        # • Lage en tom string
        streng = ''
        # • Hente ut hvert element og legge
        # til i strengen, med komma mellom
        streng = person[0] + ', ' + str(person[1]) + '\n'
        # • Skriv strengen til filen
        fil.write(streng)
    # – Lukke filen
    