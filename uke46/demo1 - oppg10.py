# her skal vi jobbe
def f(streng):
    ny = streng[0]
    for i in range(1, len(streng)):
        if streng[i] != streng[i-1]:
            ny += streng[i]
    return ny

# tester
print(f('a'))
print(f('afaaaacsdg'))
print(f('abbbdssasg'))
print(f('aabbbbcddaa'))
print(f('abbcddaabba'))
print(f('abcda'))