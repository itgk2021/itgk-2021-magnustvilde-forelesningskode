'''
Denne filen er laget som en starthjelp til eksamen
H19-kveldseksamen. Kun navn på funksjoner er gitt,
ingen parametre. Alle funksjoner begynner med
placeholder 'pass'. I samme mappe ligger 'nameregister.py'
som kan importeres slik at man kan bruke funksjoner
derifra her.
'''

import nameregister

# OPPGAVE 3a - Datosjekk
def check_date(streng):
    try:
        if len(streng) != 11:
            return False
        dag, mnd, aar = int(streng[:2]), int(streng[2:4]), int(streng[4:6])
        if dag > nameregister.days_in_month(mnd):
            return False
        if mnd > 12:
            return False
    except:
        return False
    else:
        return True

# TESTER
print(check_date('29127311245'))
print(check_date('31117532456'))
print(check_date('31137532456'))

# OPPGAVE 3b - Registrer ny fører
register = {}
people = {}

def register_person(register, people, fnr, name):
    if (fnr not in people) and (fnr not in register):
        nameregister.register_name(people, fnr, name)
        register[fnr] = []
        
# TESTER
register_person(register, people, '11010154321', 'Terje Rydland')
register_person(register, people, '23056644521', 'Børge Haugset')


# OPPGAVE 3c - Telle prikker
def count_dots(register, fnr):
    return sum(register[fnr])

# TESTER
register = {'11010154321': [3,2,3], '23056644521': [3]}
print(count_dots(register, '11010154321'))
print(count_dots(register, '23056644521'))


# OPPGAVE 3d - Legg til prikker
def add_dots(register, fnr, dots):
    register[fnr].append(dots)
    if count_dots(register, fnr) >= 10:
        print('Confiscated.')

# TESTER
print('Første:')
add_dots(register, '11010154321', 5)
print('Andre:')
add_dots(register, '23056644521', 2)

# OPPGAVE 3e - Manuell innlegging av prikker
def manual_registration(register, people):
    fnr = input('Fnr: ')
    if check_date(fnr):
        if fnr not in people:
            name = input('Navn: ')
            register_person(register, people, fnr, name)
        dots = int(input('Prikker å registrere: '))
        add_dots(register, fnr, dots)
    else:
        print('Ugyldig fnr.')

# TESTER
# Eksisterende bruker 23056644521
# manual_registration(register, people)
# # # Feil dato
# manual_registration(register, people)
# # # Ny bruker
# manual_registration(register, people)

# OPPGAVE 3f - Betale seg ut av problemer
def pay(register):
    ID = input('ID å betale for: ')
    if ID in register:
        money = int(input('How much will the driver pay: '))
        dots = money // 10000
        add_dots(register, ID, -dots)
    else:
        print('Fører er ikke registrert.')

# pay(register)

# TESTER
# register = {'11010154321': [3,2,3,2], '23056644521': [3]}
# pay(register)
# print(register['11010154321'])

# OPPGAVE 3g - Lagre til fil
def save_to_file(register, people):
    with open('dots.txt', 'w') as fil:
        for fnr in register:
            string = fnr + ' ('
            string += people[fnr] + ') har '
            string += str(count_dots(register, fnr))
            string += ' prikker.'
            fil.write(string + '\n')

# TESTER
save_to_file(register, people)