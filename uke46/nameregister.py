'''
Denne funksjonen tar inn et heltall mellom 1 og 12,
og returnerer hvor mange dager det er i denne måneden.
Funksjonen ignorerer skuddår. Funksjonen krasjer for
andre innverdier enn tallene 1-12! Eksempel:
days_in_month(1) returnerer 31 
'''
def days_in_month(month):
    return [31,28,31,30,31,30,31,31,30,31,30,31][month-1]

'''
Denne brukes til å legge til nye fødselsnummer og knytte dem
til et navn.
'''
def register_name(people, fnr, name):
    people[fnr] = name

'''
Denne funksjonen brukes til å finne et navn gitt at en oppgir
et fødselsnummer (11 siffer som streng).
'''
def find_name(people, fnr):
    return people.get(fnr, '')


''' Gjelder register_name og find_name:
Begge disse funksjonene kalles med en variabel people. Denne kan du forvente å ha
etablert i koden du skriver i alle funksjonene på denne eksamenen. For en ytterligere
beskrivelse av funksjonene, inkludert parametere, refererer vi til beskrivelsen av
modulen som ligger i bunnen av funksjonsbeskrivelsen til venstre for oppgavesettet.
Du skal altså ikke gå rett inn og endre på people, dette er kun tillatt gjennom
funksjonene i modulen.
'''