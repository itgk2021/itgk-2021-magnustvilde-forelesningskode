import numpy as np

def prosent_array(stemmer):
    returverdi = np.array(stemmer)/sum(stemmer)*100
    return np.around(returverdi, 2)

def prosent_liste(stemmer):
    ny_liste = []
    summen = sum(stemmer)
    for tall in stemmer:
        ny_liste.append(round(tall/summen*100,2))
    return ny_liste

print(prosent_array([2,7,23,89,65]))
print()
print(prosent_liste([2,7,23,89,65]))