'''praktisk bruk av arrays'''
import numpy as np

# lag to arrays a og b
a = np.array([1,2,3])
b = np.array([4,5,6])
# legg til et tall på array a
a = a + 2

# gang array b med 2 og print ut
b = b*2
print(a)
print(b)

# legg sammen de to arrayene i en ny variabel og print ut
c = a+b
print(c)

# ta np.sqrt til å finne kvadratroten til alle tallene
# i en array. Lagre i en egen variabel og skriv ut
d = np.sqrt(c)
print(d)
