'''matplotlib - søylediagram'''

aar = [2014, 2015, 2016, 2017, 2018, 2019, 2020]
avkastning = [30.35, 33.84, 11.56, 31.37, 6.62, 32.04, 24.23]

# importer modulen matplotlib.pyplot som en forkortelse
import matplotlib.pyplot as plt

# lag et søylediagram ved å bruke 'bar' sammen med de to listene over
plt.bar(aar,avkastning)

# sett ylabel til '% vekst'
# sett xlabel til 'DNB Tek Årlig'
plt.xlabel('DNB Tek Årlig')
plt.ylabel('% vekst')

# bruk metoden show() for å fremkalle diagrammet
plt.show()
