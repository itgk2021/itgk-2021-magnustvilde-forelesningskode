'''matplotlib - graf'''

aar = [2014, 2015, 2016, 2017, 2018, 2019, 2020]
vekst = [30.35, 33.84, 11.56, 31.37, 6.62, 32.04, 24.23]

# importer modulen matplotlib.pyplot som en forkortelse
import matplotlib.pyplot as plt

# bruk metoden plot sammen med de to listene over, lag et søylediagram
plt.plot(aar, vekst)

# bruk metoden show() for å fremkalle grafen
# plt.show()

# legg til beskrivelse på x- og y-aksen med xlabel() og ylabel():
#'DNB Tek årlig' på x, '% vekst' på y
plt.xlabel('DNB Tek årlig')
plt.ylabel('% vekst')
plt.show()
