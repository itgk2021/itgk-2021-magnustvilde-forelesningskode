'''numpy arrays og arange'''

# importer modulen numpy
import numpy as np

a = [1, 2, 3]
b = [4, 5, 6]
# lagre a og b arrays i to nye variabler med np.array()
c = np.array(a)
d = np.array(b)

# skriv ut listene og arrayene og se forskjellen
# mellom utskriftene
print(a)
print(b)
print(c)
print(d)

# lag en array fra -0.5 til 1.5 med steg 0.3 med arange
# og skriv ut arrayen
arr = np.arange(-0.5, 1.5, 0.3)
print(arr)
