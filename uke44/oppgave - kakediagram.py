import matplotlib.pyplot as plt

byer = ['Oslo', 'Bergen', 'Stavanger/Sandnes',
        'Trondheim', 'Fredrikstad/Sarpsborg']
innbyggere = [1036, 260, 228, 189, 116]

def lag_dia(size, tags):
    plt.pie(size, labels = tags,
            autopct = '%1.1f%%', shadow = True,
            explode = [0,0,0,0.2,0],
            startangle = 130)
    plt.show()

lag_dia(innbyggere, byer)