'''matplotlib - kakediagram'''
aar = [2014, 2015, 2016, 2017, 2018, 2019, 2020]
avkastning = [30.35, 33.84, 11.56, 31.37, 6.62, 32.04, 24.23]

# lag et kakediagram av de de to listene
import matplotlib.pyplot as plt

plt.pie(avkastning, labels = aar, autopct='%1.1f%%', explode = [0,0,0,0,0,0,0.1])
plt.show()

# vis informasjon med:
#     - labels = aar
#     - autopct='%1.1f%%'
#     - explode for å fremheve 2020

