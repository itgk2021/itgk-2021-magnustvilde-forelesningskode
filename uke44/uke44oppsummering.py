'''
Oppsummering uke 44
'''

# import av numpy for å kunne bruke numpy
import numpy as np

# lage arrays
a = np.array([1,2,3])
b = np.array([4,5,6])
c = np.array([[1,2,3],[4,5,6],[7,8,9]])

# arange
d = np.arange(0.5,1.7,0.1)

# shape, size, ndim, type
print(a.shape)
print(b.size)
print(c.ndim)
print(d.dtype)

# array med nuller
null = np.zeros(8)

# gjøre om på form (rader, kolonner)
null = null.reshape(2,4)

# array med enere, 3*3
enere = np.ones((3,3))

# regning med arrays
a = a+2
b = b*2
e = a+b

# slicing av arrays: ['slice på rader', 'slice på kolonner']
print(enere[:2, :2])

# matplotlib søyler:
import matplotlib.pyplot as plt
plt.bar(['Ola', 'Knut','Rolf'],[20,10,50])

# kakediagram
# plt.pie([20,10,50], labels = ['Ola', 'Knut','Rolf'])

# graf
# plt.plot(['Ola', 'Knut','Rolf'],[20,10,50])

# sette labels på x -og y-akse
plt.ylabel('Kr ukelønn')

# fremkalle visning
plt.show()
