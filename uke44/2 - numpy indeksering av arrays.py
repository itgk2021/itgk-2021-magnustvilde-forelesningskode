'''indeksering av arrays'''

# lag en to-dimensjonal array
import numpy as np
arr_2d = np.array([[1,2,3],[4,5,6],[7,8,9]])

# skriv ut andre element på andre rad
print(arr_2d)
print(arr_2d[1,2])
