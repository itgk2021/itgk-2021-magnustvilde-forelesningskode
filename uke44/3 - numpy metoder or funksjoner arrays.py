'''numpy - metoder og funksjoner på arrays'''

import numpy as np

# lag en to-dimensjonal array
arr = np.array([[1,2,3],[4,5,6],[7,8,9]])
liste = [[1,2,3],[4,5,6],[7,8,9]]
print(len(liste))

# bruk ndim, shape og size på arrayen og skriv ut
print(arr)
print(arr.ndim)
print(arr.shape)
print(arr.size)

# lag en 3*3 array av nuller
nuller = np.zeros(9)
nuller = nuller.reshape(3,3)
print(nuller)

null_v2 = np.zeros((3,3))
print(null_v2)