'''unntaksbehandling'''

# Lag til noe du VET gir feilmelding
# Finn feilmeldingen og håndter den med riktig type Exception.
# Skriv en beskjed til brukeren dersom det blir den typen exception.
try:
    hei
    print('Printes dette?')
except NameError:
    print('Feilmelding.')


# prøv å legge sammen to forskjellige datatyper
# håndter feilen og lagre det i en variabel  som du skriver ut
try:
    print(5 + 'min streng')
except TypeError as error:
    print('Feilmelding:', error)

# ta inn input fra bruker som du gjør om til en float og
# bruk unntaksbehandling dersom input er på feil format.
def hent_float():
    try:
        svar = float(input('Skriv inn et tall: '))
    except ValueError:
        print('Feil format.')
        svar = hent_float()
    # legg til else og finally i strukturen
    else:
        print('Riktig format.')
    finally:
        return(svar)

hent_float()