'''lese fra tekstfil'''

# vi har en tekstfil '3_tall.txt' med mange tall, og vi
# ønsker å finne gjennomsnittet av alle tallene i filen

# åpne filen
with open('3_tall.txt', 'r') as fil:
    # lagre alle tallene i en liste
    numbers = fil.readlines()
    print(numbers)
    # fjern alle '\n', slik at vi kan ta tak i tallene direkte
    for i in range(len(numbers)):
        numbers[i] = int(numbers[i].strip('\n'))
    print(numbers)
    print('Gjennomsnitt:', sum(numbers)/len(numbers))

