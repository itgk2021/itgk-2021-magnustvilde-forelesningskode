# her skal vi jobbe
def palindrom(tekst):
    for i in range(len(tekst)):
        if tekst[i] != tekst[-i-1]:
            return False
    return True

# tester
print(palindrom('aha'))
print(palindrom('abba'))
print(palindrom('sytten'))
print(palindrom('ahaa'))
print(palindrom('radar'))
print(palindrom('radarr'))
print(palindrom('raddar'))
print(palindrom('a'))