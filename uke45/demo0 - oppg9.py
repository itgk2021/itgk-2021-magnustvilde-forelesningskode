# 'sjakk.txt' ligger i mappestrukturen

# her skal vi jobbe
def file2list(filnavn):
    try:
        with open(filnavn, 'r') as fil:
            linjer = fil.readlines()
        retur = []
        for linje in linjer:
            innhold = linje.strip('\n').split(';')
            retur.append([innhold[0], innhold[1], innhold[2],
                          int(innhold[3]), int(innhold[4]),
                          float(innhold[5]), float(innhold[6])])
    except FileNotFoundError:
        print('The file with name', filnavn, 'was not found')
        retur = []
    except (ValueError, IndexError) as error:
        print('Incorrect format of data on file')
        print('Error:', error)
        retur = []
#     except IndexError:
#         print('Incorrect format of data on file')
#         retur = []
    else:
        print('Done reading the file.')
    finally:
        return retur

# tester
print(file2list('sjakk.txt'))