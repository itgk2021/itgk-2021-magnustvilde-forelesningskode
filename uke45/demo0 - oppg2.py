# fra forrige oppgave
def palindrom(tekst):
    for i in range (len(tekst)):
        if tekst[i] != tekst[-(i+1)]:
            return False
    return True

# her skal vi jobbe
def pal(setning):
    setning = setning.strip('.').replace(',', '').replace(' ', '').lower()
    return palindrom(setning)

# tester
print(pal('Ella og gjengen'))
print(pal('Ella reddet alle.'))
print(pal('Ella redder alle.'))
print(pal('Anne var i Ravenna.'))
print(pal('Anne er i Ravenna.'))