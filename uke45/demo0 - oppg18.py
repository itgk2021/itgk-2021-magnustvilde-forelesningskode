# her skal vi jobbe
def finn_ab(streng):
    if len(streng) < 3:
        return 0
    else:
        total = 0
        for i in range(len(streng)):
            try:
                if streng[i] == 'a' and streng[i+2] == 'b':
                    total +=1
            except IndexError:
                print('Utenfor indeks.')
        return total

# tester
print(finn_ab('a'))
print(finn_ab('aadd'))
print(finn_ab('acbbcabdaab'))
print(finn_ab('bbbbaccbbbb'))