# listen smed strenger som skal gjøres om
hovedsteder = ['1;China PR;Beijing;20,693,000[1];1.52%',
               '2;India;New Delhi;16,787,949[2];1.10%',
               '3;Japan;Tokyo;13,491,000[3];10.67%']

# funksjonen som refereres til i 'forrige oppgave'
def pop2int(elem):
    return int(elem[:len(elem)-3].replace(',', ''))

#her skal vi kode
def clean_list(lst):
    resultat = []
    for line in lst:
        L = line.split(';')[1:]
        L[2] = pop2int(L[2])
        L[3] = float(L[3].strip('%'))
        resultat.append(L)
    return resultat

# tester
print(clean_list(hovedsteder))