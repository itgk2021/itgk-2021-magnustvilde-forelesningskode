# her skal vi jobbe
from random import randint

def throw(n):
    tall = []
    for i in range(n):
        tall.append(randint(1,6))
    return tall

def throw_v2(n):
    return [randint(1,6) for x in range(n)]

# tester
print(throw(5))
print(throw(4))
print(throw(7))
print(throw(2))
print(throw_v2(4))