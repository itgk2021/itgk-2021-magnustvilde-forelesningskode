# funksjon fra forrige oppgave
def str2dic(streng):
    dic = {}
    for i in range(len(streng)):
        if streng[i] in dic.keys():
            dic[streng[i]].append(i)
        else:
            dic[streng[i]] = [i]
    return dic

#her skal vi jobbe
def is_ok(streng):
    dic = str2dic(streng)
    king = dic['R'][0] < dic['K'][0] and dic['K'][0] < dic['R'][1]
    bishop = dic['B'][0]%2 == 0 and dic['B'][1]%2 == 1 or dic['B'][0] % 2 == 1 and dic['B'][1]%2 == 0
    return king and bishop

# tester
print(is_ok('KRRBBQNN'))
print(is_ok('NBNBRKRQ'))
print(is_ok('NRQKBBNR'))
