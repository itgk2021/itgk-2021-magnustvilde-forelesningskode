'''Lage egne moduler'''

# importer hele 'min_modul.py' og bruk en av funksjonene som vi da får tilgang på
import min_modul
print(min_modul.veldig_langt_funksjonsnavn('Hei', 5))

# importer hele 'min_modul.py' som 'm'
import min_modul as m

# importer kun 'veldig_langt_funksjonsnavn' og kjør funksjonen
from min_modul import veldig_langt_funksjonsnavn as v
print(v('Hallo', 8))

# importer en og en funksjon som kortere navn
from min_modul import run_fibo as fibo
fibo()

