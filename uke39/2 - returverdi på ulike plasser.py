'''mer om returverdi'''

# lag en funksjon som tar inn to tall som parametre. Ta inn
# input fra bruker i form av '/', '*', '//' eller '%'.
#Funksjonen skal returnere resultatet av den typen matematisk
#operasjon gjort på de to tallene.
# def regn_ut(tall1, tall2):
#     tegn = input('Matematisk operasjon (/, *, //, %): ')
#     if tegn == '/':
#         return tall1 / tall2
#     elif tegn == '*':
#         return tall1 * tall2
#     elif tegn == '//':
#         return tall1 // tall2
#     elif tegn == '%':
#         return tall1 % tall2
# 
# print(regn_ut(64,34))
# print(regn_ut(70,34))

# lag en funksjon som tar inn en streng. Iterer over strengen og
# returner den første vokalen du finner. Dersom det ikke finnes
# noen vokal i strengen, returner en beskrivende tekst
vokal = 'aeiouyæøå'
def finn_vokal(streng):
    for bostav in streng:
        if bostav.lower() in vokal:
            return bostav
    return 'Ingen vokal i strengen.'

print(finn_vokal('Min streng'))
