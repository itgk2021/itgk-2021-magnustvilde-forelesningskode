'''Moduler og import'''

# importer modulen math og regn ut kvadratroten av et tall
import math
print(math.sqrt(85))

# importer modulen random og lag til tre tilfeldige flyttall
# from random import random
# print(random())
# print(random())
# print(random())

vokaler = 'aeiouyæøå'
konsonanter = 'bcdfghjklmnpqrstvwxz'
# Lag en funksjon 'ordgenerator' som tar inn en parameter lengde. Returner
# et ord som begynner på en tilfeldig konsonant, og har vokal som annenhver
# bokstav
from random import randint

def ordgenerator(lengde):
    streng = ''
    for i in range(lengde):
        if i % 2 == 0:
            streng += konsonanter[randint(0,len(konsonanter)-1)]
        else:
            streng += vokaler[randint(0,len(vokaler)-1)]
    return streng

print(ordgenerator(4))
print(ordgenerator(6))
#skriv  ut resultatet av ordgenerator(4)