'''Endre på lister'''

# opprett en liste med 10 elementer
liste = [1,2,3,4,5,6,7,8,9,10]

# endre elementet på indeks 5 til 'Endret' og skriv ut listen
liste[5] = 'Endret'
print(liste)

# legg til ett element på slutten av listen og skriv ut listen
liste.append('Slutt')
print(liste)

# legg til ett element på indeks fem (ingen elementer skal bli overskrevet)
liste.insert(5, 'Lagt til')
print(liste)

# fjern det andre elementet i listen ved bruk av del
del liste[1]
print(liste)

# fjern det siste elementet i listen ved bruk av pop()
a = liste.pop()
print(liste)
print(f'a: {a}')
