'''slicing av strenger'''

# lag en streng og skriv ut de første
# fire tegnene i strengen
streng = 'Dette er min streng'
print(streng[:4])

# skriv ut annenhvert tegn i strengen vha. slicing
print(streng[::2])

# legg til en bokstav midt i strengen
streng = streng[:6] + 'æ' + streng[6:]
print(streng)

# legg til navnet ditt på indeks 3 i strengen
streng = streng[:3] + 'Magnus' + streng[3:]
print(streng)

