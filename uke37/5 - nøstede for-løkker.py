'''nøstede for-løkker'''

# lag et program som skriver ut tallene fra 1 til og med 9 i et 'rutenett':
'''
1    2    3
4    5    6
7    8    9
'''
for i in range(1,4):
    streng1 = ''
    for j in range(1,4):
        streng1+= str(i*j)+'\t'
    print(streng1)
