'''Mengdeoperasjoner på dictionaries'''

# lag tre mengder a, b og c som inneholder tall
a = set([1,2,3])
b = set([3,4,5])
c = set([4,5])

# finn snittet (union) av mengdene
print(a.union(b))

# finn hvilke verdier som er inneholdt i begge mengdene med intersection()
print(b.intersection(c))

# sjekk hva a har som ikke b har med difference()
print(a.difference(b))
print(b.difference(a))

# sjekk om a er subset av c
print(b.issubset(c))

#sjekk om a er superset av c
print(b.issuperset(c))
