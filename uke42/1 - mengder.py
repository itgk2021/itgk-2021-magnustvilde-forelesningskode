'''Mengder (sets)'''

# lag en tom mengde med set()
mengde = set()

# legg inn noen verdier der det finnes duplikater med metoden add()
mengde.add(3)
mengde.add(4)
mengde.add(4)
mengde.add(5)
mengde.add(5)

# legg til en liste av verdier i settet med metoden update()
mengde.update([5,6,7,8])
print(mengde)

# sjekk om en verdi er inneholdt i mengden
a = 10 in mengde
print(a)

# slett en verdi fra den første mengden
mengde.remove(5)
print(mengde)
