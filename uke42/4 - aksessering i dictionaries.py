'''Aksessering i dictionaries'''

person = {'name': 'Alex', 'age': 17, 'drivers license': False,
          'height': 175, 'sex': 'non-binary'}

# skriv ut verdien som er tilknyttet nøkkelen 'age'
print(person['age'])

# skriv ut verdien som er tilknyttet nøkkelen 'height'
print(person['height'])

# spør brukeren om hva brukeren ønsker å vite om personen, og skriv ut det
# brukeren spør om (må være en nøkkel de spør om)
svar = input('Hva ønsker du å vite: ').lower()
print(person[svar])

# skriv ut en nøkkel, men slik at første bokstav er stor (streng-manipulering)

